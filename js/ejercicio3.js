

$(document).on("click", "#menos", function() {
    var numero = parseInt($("input#valor").val());
    if (numero > 0) {
        $("input#valor").val(numero-1);
    } else if (numero > 10) {
        $("input#valor").val("10");
    } else {
        $("input#valor").val("0");
    }
})

$(document).on("click", "#mas", function() {
    var numero = parseInt($("input#valor").val());
    if (numero < 10) {
        $("input#valor").val(numero+1);
    } else if (numero < 0) {
        $("input#valor").val("0");
    } else {
        $("input#valor").val("10");
    }
})

$(document).on("click", "#randNum", function() {
    $(".rand").each(function() {
        var randomNum = Math.floor(Math.random()*50);
        $(this).text(randomNum);
    })
})

$(document).on("click", "#suma", function() {
    var pages = new Array();
    $(".pag").each(function() {
        pages.push(parseInt($(this).text()));
    })
    if (pages[pages.length-1] < 20) {
        $(".pag").each(function(i){
            var newPage = pages[i]+1
            $(this).attr("href","p"+newPage+".html").text(newPage);
        })
    }
})

$(document).on("click", "#resta", function() {
    var pages = new Array();
    $(".pag").each(function() {
        pages.push(parseInt($(this).text()));
    })
    if (pages[0] > 0) {
        $(".pag").each(function(i){
            var newPage = pages[i]-1
            $(this).attr("href","p"+newPage+".html").text(newPage);
        })
    }
})