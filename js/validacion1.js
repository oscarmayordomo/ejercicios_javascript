

var enviarFormulario = true;

// Validación nombre

$(document).on("submit", "#formulario", function (evento) {
    var name = $("input#nombre");
    var nameValue = name.val();
    if (nameValue === "") {
        name.addClass("is-invalid");
        name.removeClass("is-valid");
        $("div#invalidNombre").text("Campo requerido");
        enviarFormulario = false;
    } else {
        name.addClass("is-valid");
        name.removeClass("is-invalid");
    }
})

// Validación apellido

$(document).on("submit", "#formulario", function(evento){
    var lastName = $("input#apellido");
    var lastNameValue = lastName.val();
    if (lastNameValue === "") {
        lastName.removeClass("is-valid");
    } else {
        lastName.addClass ("is-valid");
    }
})

// Validación email

$(document).on("submit", "#formulario", function (evento) {
    var email = $("input#email");
    var emailValue = email.val();
    var validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // var validEmail = /^[a-zA-Z-0-9!#$%&'*+\-\/=?^_`{|}~\.]+@[a-zA-Z_]+(\.[a-zA-Z]{2,3}){1,2}$/;
    if (emailValue === "") {
        email.addClass("is-invalid");
        email.removeClass("is-valid");
        $("div#invalidEmail").text("Campo requerido");
        enviarFormulario = false;
    } else if (!(emailValue.match(validEmail))) {
        email.addClass("is-invalid");
        email.removeClass("is-valid");
        $("div#invalidEmail").text("Introduce un email válido");
        enviarFormulario = false;
    } else {
        email.addClass("is-valid");
        email.removeClass("is-invalid");
    }
})

// Validación contraseña

$(document).on("submit", "#formulario", function (evento) {
    var password1 = $("input#password1");
    var password1Value = password1.val();
    var password2 = $("input#password2");
    var password2Value = password2.val();
    var validPassword = /^(?=\S*?[a-z])(?=\S*?[0-9])\S{8,}$/;
    if (password1Value === "") {
        password1.addClass("is-invalid");
        password1.removeClass("is-valid");
        $("div#invalidPassword1").text("Campo requerido");
        password2.addClass("is-invalid");
        password2.removeClass("is-valid");
        $("div#invalidPassword2").text("");
        enviarFormulario = false;
    } else if (password1Value.match(/\s+/)) {
        password1.addClass("is-invalid");
        password1.removeClass("is-valid");
        $("div#invalidPassword1").text("La contraseña no debe contener espacios");
        password2.addClass("is-invalid");
        password2.removeClass("is-valid");
        $("div#invalidPassword2").text("");
        enviarFormulario = false;
    } else if (password1Value.match(/^.{1,7}$/)) {
        password1.addClass("is-invalid");
        password1.removeClass("is-valid");
        $("div#invalidPassword1").text("La contraseña debe tener al menos 8 caracteres");
        password2.addClass("is-invalid");
        password2.removeClass("is-valid");
        $("div#invalidPassword2").text("");
        enviarFormulario = false;
    } else if (!(password1Value.match(validPassword))) {
        password1.addClass("is-invalid");
        password1.removeClass("is-valid");
        $("div#invalidPassword1").text("La contraseña debe contener al menos una letra y un número");
        password2.addClass("is-invalid");
        password2.removeClass("is-valid");
        $("div#invalidPassword2").text("");
        enviarFormulario = false;
    } else if (password2Value !== password1Value) {
        password1.addClass("is-invalid");
        password1.removeClass("is-valid");
        $("div#invalidPassword1").text("");
        password2.addClass("is-invalid");
        password2.removeClass("is-valid");
        $("div#invalidPassword2").text("Las contraseñas no coinciden");
        enviarFormulario = false;
    } else {
        password1.addClass("is-valid");
        password1.removeClass("is-invalid");
        password2.addClass("is-valid");
        password2.removeClass("is-invalid");
    }
})

// Validación fecha nacimiento

$(document).on("submit", "#formulario", function (evento) {
    var bornDate = $("input#fechaNacimiento");
    var bornDateValue = new Date(bornDate.val());
    var currentDate = new Date();
    if (bornDateValue == "Invalid Date") {
        bornDate.addClass("is-invalid");
        bornDate.removeClass("is-valid");
        $("div#invalidFechaNacimiento").text("Introduce una fecha válida");
        enviarFormulario = false;
    } else if (bornDateValue.getTime() > currentDate.getTime()) {
        bornDate.addClass("is-invalid");
        bornDate.removeClass("is-valid");
        $("div#invalidFechaNacimiento").text("Vienes del futuro?");
        enviarFormulario = false;
    } else {
        var bornYear = bornDateValue.getFullYear();
        bornDateValue.setFullYear(bornYear + 18);
        if (bornDateValue.getTime() > currentDate.getTime()) {
            bornDate.addClass("is-invalid");
            bornDate.removeClass("is-valid");
            $("div#invalidFechaNacimiento").text("Debes ser mayor de 18 años");
            enviarFormulario = false;
        } else {
            bornDate.addClass("is-valid");
            bornDate.removeClass("is-invalid");
        }
    }
})

$(document).on("submit", "#formulario", function (evento) {
    if (!enviarFormulario) {
        evento.preventDefault();
    }
})