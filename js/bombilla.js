

// $('input[type=range]').on('input', function () {
//     $(this).trigger('change');
// });

$(document).on("click", ".switch", function() {
    var intensidad = ($("#intensidad").val())/100;
    $("div.switch img").each(function() {
        if ($(this).hasClass("ocultar")) {
            $(this).removeClass("ocultar")
        } else if (!$(this).hasClass("ocultar")) {
            $(this).addClass("ocultar")
        }
    })

    // $("div.bombilla img").each(function() {
    //     $(this).fadeToggle(500);
    // })

    if ($("#switch-off").hasClass("ocultar")) {
        $("#bombilla-on").fadeTo(500, intensidad);
        $("#bombilla-off").fadeTo(500, 100-intensidad);
    } else {
        $("#bombilla-on").fadeOut(500);
        $("#bombilla-off").fadeIn(500);
    }
})

$(document).on("input", "#intensidad", function() {
    var intensidad = ($(this).val())/100;
    if ($("#switch-off").hasClass("ocultar")) {
        $("#bombilla-on").fadeTo(20, intensidad);
        $("#bombilla-off").fadeTo(20, 100-intensidad);
    }
})